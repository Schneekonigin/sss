﻿using UnityEngine;

public class Event : MonoBehaviour
{
    [TextArea(3, 10)] public string Note; // only for notes on the event to keep an overview
    public string trigger;
    public bool replay;

    void Start()
    {
        EventManager.StartListening(trigger, doEvent);
    }
    virtual public void doEvent()
    {

        StateMachine.Instance.state[trigger] = replay;
    }
    public void StateTrigger()
    {
        doEvent();        
    }

    private void OnDestroy()
    {
        EventManager.StopListening(trigger, doEvent);
    }
}