﻿using UnityEngine;

public class EventCollector : MonoBehaviour
{
    private Event[] collector;
    void Awake()
    {
        collector = gameObject.GetComponents<Event>();// collects all Events on the Gameobject and puts them in a Dictionary      
    }
    private void Start()
    {
        foreach (Event evnt in collector)
        {
            if (StateMachine.Instance.state.TryGetValue(evnt.trigger, out bool val))
            {
                if (val)
                {
                    evnt.StateTrigger();
                }
            }

        }
    }
}
