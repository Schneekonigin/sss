﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateMachine : MonoBehaviour
{
    public Dictionary<string, bool> state;
    public List<bool> stateB;
    public List<string> stateS;
    private static StateMachine instance;
    public static StateMachine Instance
    {
        get
        {
            if (!instance)
            {
                instance = FindObjectOfType<StateMachine>();
                if (!instance)
                {
                    Debug.LogError("No StateMachine in scene");
                }
                else
                {
                    instance.Init();
                }
            }

            return instance;
        }
    }
    private void Init()
    {
       state = new Dictionary<string, bool>();
        stateB = new List<bool>();
        stateS = new List<string>();
    }

    public void saveState(string key)
    {
        instance.state.Add(key, true);
        instance.stateB.Add( true);
        instance.stateS.Add(key);
    }
}
