﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

public class EventManager : MonoBehaviour
{

    private Dictionary<string, UnityEvent> eventDictionary; // dictionary so that a string can be used to directly access an entry without the need to iterate through it
    public List<string> stateS;
    private Dictionary<string, bool> stateMachine;
    private static EventManager eventManager;
    public static EventManager instance // singleton the object manager is searched in the scene and an error is sended if none is to be found. Needed to be able to call the eventmanager without getComponent everytime.
    {
        get
        {
            if (!eventManager)
            {
                eventManager = FindObjectOfType(typeof(EventManager)) as EventManager;

                if (!eventManager)
                {
                    Debug.LogError("No EventManager in scene");
                }
                else
                {
                    eventManager.Init();
                }
            }

            return eventManager;
        }
    }

    void Init() // dictionary is created if there isnt one yet
    {
        if (eventDictionary == null)
        {
            eventDictionary = new Dictionary<string, UnityEvent>();
            stateS = new List<string>();
        }
    }

    public static void StartListening(string eventName, UnityAction listener) // listener is set refering to a string and a method. If the event already exists it adda a listner to the method and if the event does not exist yet it creates a nre event in the dictionary and addws the listener then
    {
        UnityEvent thisEvent = null;
        if (instance.eventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.AddListener(listener);
        }
        else
        {
            thisEvent = new UnityEvent();
            thisEvent.AddListener(listener);
            instance.eventDictionary.Add(eventName, thisEvent);
            instance.stateS.Add(eventName);
        }
    }

    public static void StopListening(string eventName, UnityAction listener) //removes the listener from a method
    {
        if (eventManager == null) return;
        UnityEvent thisEvent = null;
        if (instance.eventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.RemoveListener(listener);
        }
    }

    public static void TriggerEvent(string eventName) // gets the event from the dictionary and invokes it as soon as its triggerd.
    {
        UnityEvent thisEvent = null;
        if (instance.eventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.Invoke();
        }
    }

    public static void DoEvent(string eventName) // gets the event from the dictionary and invokes it as soon as its triggerd.
    {
        UnityEvent thisEvent = null;
        if (instance.eventDictionary.TryGetValue(eventName, out thisEvent))// if Listerner is in scene
        {
            if (!StateMachine.Instance.state.TryGetValue(eventName, out bool idc))
            {
                StateMachine.Instance.saveState(eventName);
            }
            thisEvent.Invoke();
        }

    }
}