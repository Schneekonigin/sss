﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmallSpaceShuttle : MonoBehaviour
{
    public ParticleSystem[] particles;
    public Rigidbody body;

    public int floatingTime = 15;
    public float force = 4;

   
    public void onGrab()
    {
        foreach(ParticleSystem system in particles)
        {
            system.Play();
        }
        
    }
    public void onRelease()
    {
        Invoke("Stop", floatingTime);
        body.useGravity = false;
        body.AddForce(0, 0, force);

    }
    public void Stop()
    {
        foreach (ParticleSystem system in particles)
        {
            system.Stop();
        }
        body.useGravity = true;
    }
}
